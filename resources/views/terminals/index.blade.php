@extends('layouts.app')
@section('content')

    <div id="wrapper">
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">
                    <a class="text-center" href="#">
                        {{$terminal->TerminalMeta[0]->terminal_name}} :&nbsp;
                        <terminal-time></terminal-time>
                    </a>
                </li>
                <li><a href="#">Op Number : {{$employee[0]->id}}</a></li>
                <li><a href="#">Op Name : {{$employee[0]->first_name}} {{$employee[0]->surname}}</a></li>
                <li><a href="#">Time Logged : <terminal-elapsed-work-time  :employeedata="{{ json_encode($employee[0]) }}"></terminal-elapsed-work-time></a></li>
                <li><a class="text-center" href="#">
                        <button class="btn btn-danger btn-block btn-lg text-center" data-toggle="modal" data-target="#exampleModal">Switch Operator
                        </button>&nbsp;</a>
                </li>
            </ul>

        </div>
        <div class="page-content-wrapper">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3>&nbsp;<br>{{$terminal->TerminalMeta[0]->terminal_name}}<br><br></h3>
                        </div>
                    </div>
                </div>
                <terminal-data :terminal-data="{{ json_encode($terminal) }}"></terminal-data>
            </div>
        </div>
        <terminal-messages :terminal-data="{{ json_encode($terminal) }}"></terminal-messages>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Switch Employee</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <terminal-login :currentemployee="{{ json_encode($employee[0]) }}"></terminal-login>
                </div>
                <div class="modal-footer">
                    <form class="form-signin" method="post" action="/terminal/logout">
                        @csrf
                        <input type="hidden" id="Employee" name="Employee" value="{{$employee[0]->id}}">

                        <button class="btn btn-lg btn-primary btn-block" type="submit">Log out</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection


