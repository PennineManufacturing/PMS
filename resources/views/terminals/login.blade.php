@extends('layouts.app')

@section('content')

@php



@endphp

    <div class="container">

        <form class="form-signin" method="post" action="{{ url('terminal/login') }}">
            @csrf
            <h1 class="h3 mb-3 text-center">Please sign in below</h1>
            <label for="inputEmail" class="sr-only">Email address</label>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Employee</label>
                <select name="Employee" class="form-control" id="">
                    @foreach ($employees as $employee)
                        <option value="{{$employee->id}}">{{$employee->first_name }}   {{$employee->surname}}</option>

                    @endforeach
                </select>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
        </form>
    {{\Request::ip()}}
    </div>


@endsection
