@php


@endphp

<div class="">
    {{ Session::get('message') }}
</div>

<div class="container">

    {!! Form::model($terminal, ['route' => ['terminals.update', $terminal->id], 'method' => 'patch']) !!}

    @form_maker_object($terminal, FormMaker::getTableColumns('terminals'))

    {!! Form::submit('Update') !!}

    {!! Form::close() !!}
</div>
