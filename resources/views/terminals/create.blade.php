


@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        <div class="">
                            {{ Session::get('message') }}
                        </div>

                        <div class="container">

                            {!! Form::open(['route' => 'terminals.store']) !!}

                            @form_maker_table("terminals")

                            {!! Form::submit('Save') !!}

                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
