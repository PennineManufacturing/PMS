
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.moment = require('moment');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'terminal-time',
    require('./components/terminal/TimeComponent.vue').default
);

Vue.component(
    'terminal-data',
    require('./components/terminal/TerminaDataComponent.vue').default
);

Vue.component(
    'terminal-login',
    require('./components/terminal/LoginComponent.vue').default
);

Vue.component(
    'terminal-elapsed-work-time',
    require('./components/terminal/ElapsedWorkTimeComponent.vue').default
);

Vue.component(
    'terminal-messages',
    require('./components/terminal/TerminaMessageComponent.vue').default
);

Vue.component(
    'terminal-swipe',
    require('./components/terminal/SwipeComponent.vue').default
);

const app = new Vue({
    el: '#app'
});

