<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Terminal\TerminalController@index')->name('terminalHome');

Route::get('api', function () {
    return view('api');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');



Route::get('/terminal', 'Terminal\TerminalController@index')->name('terminalHome');
Route::post('/terminal/login', 'Terminal\TerminalController@login')->name('terminalLogin');
Route::post('/terminal/logout', 'Terminal\TerminalController@logout')->name('terminalLogout');
Route::post('/terminal/changeEmployee', 'Terminal\TerminalController@change_user')->name('TerminalChange');

