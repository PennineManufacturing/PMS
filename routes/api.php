<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(array('prefix' => 'v1', 'middleware' => []), function () {
    Route::group(array('prefix' => 'terminal', 'middleware' => []), function () {
        Route::middleware('auth:api')->get('/unused', 'APITerminalsController@index');
        Route::middleware('auth:api')->get('/claim/{terminalID}', 'APITerminalsController@claim');
        Route::middleware('auth:api')->post('/update/{terminalID}', 'APITerminalsController@update');
        Route::middleware('auth:api')->post('/heartbeat/{terminalID}', 'APITerminalsController@heartbeat');
        Route::middleware('api')->get('/data/{terminalID}', 'APITerminalsController@getTerminalData');
        Route::middleware('api')->get('/swipe/{terminalID}', 'APITerminalsController@getSwipeData');
        Route::middleware('api')->get('/login', 'APITerminalsController@login');
        Route::middleware('api')->get('/data/message/{terminalID}', 'APITerminalsController@getTerminalMessage');
    });

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user();
    });
    //Route::middleware('auth:api')->get('all', 'EmployeeController@getAllEmployees');
});


