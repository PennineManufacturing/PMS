<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TerminalAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Terminal = factory(App\Models\Terminal::class)->make([
            // Terminal table data
        ]);
        $this->TerminalEdited = factory(App\Models\Terminal::class)->make([
            // Terminal table data
        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/terminals');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/terminals', $this->Terminal->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/terminals', $this->Terminal->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/terminals/1', $this->TerminalEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('terminals', $this->TerminalEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/terminals', $this->Terminal->toArray());
        $response = $this->call('DELETE', 'api/v1/terminals/'.$this->Terminal->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'terminal was deleted']);
    }

}
