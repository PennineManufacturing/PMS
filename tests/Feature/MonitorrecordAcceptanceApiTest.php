<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MonitorrecordAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Monitorrecord = factory(App\Models\Monitorrecord::class)->make([
            'id' => '1',
		'record_type' => 'nisi',
		'record_data' => 'quaerat',
		'terminal_id' => '1',
		'created_at' => '2019-02-15 01:48:20',
		'updated_at' => '2019-02-15 01:48:20',
		'deleted_at' => '2019-02-15 01:48:20',

        ]);
        $this->MonitorrecordEdited = factory(App\Models\Monitorrecord::class)->make([
            'id' => '1',
		'record_type' => 'nisi',
		'record_data' => 'quaerat',
		'terminal_id' => '1',
		'created_at' => '2019-02-15 01:48:20',
		'updated_at' => '2019-02-15 01:48:20',
		'deleted_at' => '2019-02-15 01:48:20',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/monitorrecords');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/monitorrecords', $this->Monitorrecord->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/monitorrecords', $this->Monitorrecord->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/monitorrecords/1', $this->MonitorrecordEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertDatabaseHas('monitorrecords', $this->MonitorrecordEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/monitorrecords', $this->Monitorrecord->toArray());
        $response = $this->call('DELETE', 'api/v1/monitorrecords/'.$this->Monitorrecord->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'monitorrecord was deleted']);
    }

}
