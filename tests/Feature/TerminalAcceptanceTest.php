<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TerminalAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Terminal = factory(App\Models\Terminal::class)->make([
            // Terminal table data
        ]);
        $this->TerminalEdited = factory(App\Models\Terminal::class)->make([
            // Terminal table data
        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'terminals');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('terminals');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'terminals/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'terminals', $this->Terminal->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('terminals/'.$this->Terminal->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'terminals', $this->Terminal->toArray());

        $response = $this->actor->call('GET', '/terminals/'.$this->Terminal->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('terminal');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'terminals', $this->Terminal->toArray());
        $response = $this->actor->call('PATCH', 'terminals/1', $this->TerminalEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('terminals', $this->TerminalEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'terminals', $this->Terminal->toArray());

        $response = $this->call('DELETE', 'terminals/'.$this->Terminal->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('terminals');
    }

}
