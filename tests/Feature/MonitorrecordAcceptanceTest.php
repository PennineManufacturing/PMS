<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class MonitorrecordAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->Monitorrecord = factory(App\Models\Monitorrecord::class)->make([
            'id' => '1',
		'record_type' => 'sit',
		'record_data' => 'enim',
		'terminal_id' => '1',
		'created_at' => '2019-02-15 01:48:20',
		'updated_at' => '2019-02-15 01:48:20',
		'deleted_at' => '2019-02-15 01:48:20',

        ]);
        $this->MonitorrecordEdited = factory(App\Models\Monitorrecord::class)->make([
            'id' => '1',
		'record_type' => 'sit',
		'record_data' => 'enim',
		'terminal_id' => '1',
		'created_at' => '2019-02-15 01:48:20',
		'updated_at' => '2019-02-15 01:48:20',
		'deleted_at' => '2019-02-15 01:48:20',

        ]);
        $user = factory(App\Models\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'monitorrecords');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('monitorrecords');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'monitorrecords/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'monitorrecords', $this->Monitorrecord->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('monitorrecords/'.$this->Monitorrecord->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'monitorrecords', $this->Monitorrecord->toArray());

        $response = $this->actor->call('GET', '/monitorrecords/'.$this->Monitorrecord->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('monitorrecord');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'monitorrecords', $this->Monitorrecord->toArray());
        $response = $this->actor->call('PATCH', 'monitorrecords/1', $this->MonitorrecordEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertDatabaseHas('monitorrecords', $this->MonitorrecordEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'monitorrecords', $this->Monitorrecord->toArray());

        $response = $this->call('DELETE', 'monitorrecords/'.$this->Monitorrecord->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('monitorrecords');
    }

}
