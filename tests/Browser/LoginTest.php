<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('email', 'Jkirkpatrick@penninemanufacturing.local')
                ->type('password', 'Aeqpzmuc1')
                ->press('Log In')
                ->assertPathIs('/admin');
        });
    }
}
