<?php
/*
|--------------------------------------------------------------------------
| Monitorrecord Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Monitorrecord::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'record_type' => 'ut',
		'record_data' => 'voluptatibus',
		'terminal_id' => '1',
		'created_at' => '2019-02-15 01:48:20',
		'updated_at' => '2019-02-15 01:48:20',
		'deleted_at' => '2019-02-15 01:48:20',
    ];
});
