<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateTerminalMetaTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('terminalmeta',function(Blueprint $table){
            $table->increments("id");
            $table->enum("terminal_type", ["Access", "Monitor"]);
            $table->string("terminal_name")->nullable();
            $table->text("location")->nullable();
            $table->date("heartbeat")->nullable();
            $table->integer("terminal_id")->references("id")->on("terminals");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('terminalmeta');
    }

}