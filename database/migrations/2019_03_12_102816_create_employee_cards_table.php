<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateEmployeeCardsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('employeecards',function(Blueprint $table){
            $table->increments("id");
            $table->string("card_id")->nullable();
            $table->string("card_type")->nullable();
            $table->integer("employee_id")->references("id")->on("employee")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employeecards');
    }

}