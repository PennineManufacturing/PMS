<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateMonitorRecordTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Model::unguard();
        Schema::create('monitorrecord',function(Blueprint $table){
            $table->increments("id");
            $table->enum("record_type", ["Shot", "Scrap", "Heat", "Pump", "Swipe", "Login", "Logout"]);
            $table->string("record_data")->nullable();
            $table->integer("terminal_id")->references("id")->on("terminals");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('monitorrecord');
    }

}