<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Laraveldaily\Quickadmin\Models\Menu::insert([
            [
                'name'      => 'User',
                'title'     => 'User',
                'menu_type' => 0
            ],
            [
                'name'      => 'Role',
                'title'     => 'Role',
                'menu_type' => 0
            ]
        ]);

    }
}
