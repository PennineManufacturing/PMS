<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'employee';
    
    protected $fillable = [
          'terminals_id',
          'card_code',
          'first_name',
          'middle_name',
          'surname'
    ];
    

    public static function boot()
    {
        parent::boot();

        Employee::observe(new UserActionsObserver);
    }
    
    public function terminals()
    {
        return $this->hasOne('App\Terminals', 'id', 'terminals_id');
    }


    
    
    
}