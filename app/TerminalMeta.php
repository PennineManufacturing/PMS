<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

use Carbon\Carbon; 

use Illuminate\Database\Eloquent\SoftDeletes;

class TerminalMeta extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'terminalmeta';
    
    protected $fillable = [
          'terminal_type',
          'location',
          'terminal_id'
    ];
    
    public static $terminal_type = ["Access" => "Access", "Monitor" => "Monitor"];


    public static function boot()
    {
        parent::boot();

        TerminalMeta::observe(new UserActionsObserver);
    }
    
    public function terminals()
    {
        return $this->hasOne('App\Terminals', 'id', 'terminal_id');
    }





    
}