<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

use Carbon\Carbon; 

use Illuminate\Database\Eloquent\SoftDeletes;

class Terminals extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'terminals';
    
    protected $fillable = [
          'hostname',
          'ip_address',
          'mac_address',
          'assigned'
    ];
    

    public static function boot()
    {
        parent::boot();

        Terminals::observe(new UserActionsObserver);
    }
    
    
    /**
     * Set attribute to date format
     * @param $input
     */
    public function setheartbeatAttribute($input)
    {
        if($input != '') {
            $this->attributes['heartbeat'] = Carbon::createFromFormat(config('quickadmin.date_format'), $input)->format('Y-m-d');
        }else{
            $this->attributes['heartbeat'] = '';
        }
    }

    public function MonitorRecord()
    {
        return $this->hasMany('App\MonitorRecord', 'terminal_id', 'id');
    }

    public function TerminalMeta()
    {
        return $this->hasMany('App\TerminalMeta', 'terminal_id', 'id');
    }

    public function Employee()
    {
        return $this->hasOne('App\Employee', 'terminals_id', 'id');
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getheartbeatAttribute($input)
    {
        if($input != '0000-00-00') {
            return Carbon::createFromFormat('Y-m-d', $input)->format(config('quickadmin.date_format'));
        }else{
            return '';
        }
    }


    
}