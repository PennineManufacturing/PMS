<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class MonitorRecord extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'monitorrecord';
    
    protected $fillable = [
          'record_type',
          'record_data',
          'terminal_id'
    ];
    
    public static $record_type = ["Shot" => "Shot", "Heat" => "Heat", "Swipe" => "Swipe", "Scrap" => "Scrap"];


    public static function boot()
    {
        parent::boot();

        MonitorRecord::observe(new UserActionsObserver);
    }
    
    public function terminals()
    {
        return $this->hasOne('App\Terminals', 'id', 'terminals_id');
    }


    
    
    
}