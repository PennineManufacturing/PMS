<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeCards extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'employeecards';
    
    protected $fillable = [
          'card_id',
          'card_type',
          'employee_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        EmployeeCards::observe(new UserActionsObserver);
    }
    
    public function employee()
    {
        return $this->hasMany('App\Employee', 'id', 'employee_id');
    }


    
    
    
}