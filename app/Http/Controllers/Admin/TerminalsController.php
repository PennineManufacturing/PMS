<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Terminals;
use App\Http\Requests\CreateTerminalsRequest;
use App\Http\Requests\UpdateTerminalsRequest;
use Illuminate\Http\Request;



class TerminalsController extends Controller {

	/**
	 * Display a listing of terminals
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $terminals = Terminals::all();

		return view('admin.terminals.index', compact('terminals'));
	}

	/**
	 * Show the form for creating a new terminals
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.terminals.create');
	}

	/**
	 * Store a newly created terminals in storage.
	 *
     * @param CreateTerminalsRequest|Request $request
	 */
	public function store(CreateTerminalsRequest $request)
	{
		Terminals::create($request->all());

		return redirect()->route(config('quickadmin.route').'.terminals.index');
	}

	/**
	 * Show the form for editing the specified terminals.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$terminals = Terminals::find($id);
	    
	    
		return view('admin.terminals.edit', compact('terminals'));
	}

	/**
	 * Update the specified terminals in storage.
     * @param UpdateTerminalsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTerminalsRequest $request)
	{
		$terminals = Terminals::findOrFail($id);

        

		$terminals->update($request->all());

		return redirect()->route(config('quickadmin.route').'.terminals.index');
	}

	/**
	 * Remove the specified terminals from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Terminals::destroy($id);

		return redirect()->route(config('quickadmin.route').'.terminals.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Terminals::destroy($toDelete);
        } else {
            Terminals::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.terminals.index');
    }

}
