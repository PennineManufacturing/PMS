<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Terminals;
use Redirect;
use Schema;
use App\TerminalMessage;
use App\Http\Requests\CreateTerminalMessageRequest;
use App\Http\Requests\UpdateTerminalMessageRequest;
use Illuminate\Http\Request;
use Symfony\Component\Console\Terminal;


class TerminalMessageController extends Controller {

	/**
	 * Display a listing of terminalmessage
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $terminalmessage = TerminalMessage::all();

		return view('admin.terminalmessage.index', compact('terminalmessage'));
	}

	/**
	 * Show the form for creating a new terminalmessage
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
    {
        $terminals = Terminals::pluck("id", "id")->prepend('Please select', 0);
	    
	    return view('admin.terminalmessage.create',compact('terminals'));
	}

	/**
	 * Store a newly created terminalmessage in storage.
	 *
     * @param CreateTerminalMessageRequest|Request $request
	 */
	public function store(CreateTerminalMessageRequest $request)
	{
	    
		TerminalMessage::create($request->all());

		return redirect()->route(config('quickadmin.route').'.terminalmessage.index');
	}

	/**
	 * Show the form for editing the specified terminalmessage.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$terminalmessage = TerminalMessage::find($id);
	    
	    
		return view('admin.terminalmessage.edit', compact('terminalmessage'));
	}

	/**
	 * Update the specified terminalmessage in storage.
     * @param UpdateTerminalMessageRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTerminalMessageRequest $request)
	{
		$terminalmessage = TerminalMessage::findOrFail($id);

        

		$terminalmessage->update($request->all());

		return redirect()->route(config('quickadmin.route').'.terminalmessage.index');
	}

	/**
	 * Remove the specified terminalmessage from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TerminalMessage::destroy($id);

		return redirect()->route(config('quickadmin.route').'.terminalmessage.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TerminalMessage::destroy($toDelete);
        } else {
            TerminalMessage::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.terminalmessage.index');
    }

}
