<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\MonitorRecord;
use App\Http\Requests\CreateMonitorRecordRequest;
use App\Http\Requests\UpdateMonitorRecordRequest;
use Illuminate\Http\Request;

use App\Terminals;


class MonitorRecordController extends Controller {

	/**
	 * Display a listing of monitorrecord
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $monitorrecord = MonitorRecord::with("terminals")->get();

		return view('admin.monitorrecord.index', compact('monitorrecord'));
	}

	/**
	 * Show the form for creating a new monitorrecord
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $terminals = Terminals::pluck("id", "id")->prepend('Please select', 0);

	    
        $record_type = MonitorRecord::$record_type;

	    return view('admin.monitorrecord.create', compact("terminals", "record_type"));
	}

	/**
	 * Store a newly created monitorrecord in storage.
	 *
     * @param CreateMonitorRecordRequest|Request $request
	 */
	public function store(CreateMonitorRecordRequest $request)
	{
	    
		MonitorRecord::create($request->all());

		return redirect()->route(config('quickadmin.route').'.monitorrecord.index');
	}

	/**
	 * Show the form for editing the specified monitorrecord.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$monitorrecord = MonitorRecord::find($id);
	    $terminals = Terminals::pluck("id", "id")->prepend('Please select', 0);

	    
        $record_type = MonitorRecord::$record_type;

		return view('admin.monitorrecord.edit', compact('monitorrecord', "terminals", "record_type"));
	}

	/**
	 * Update the specified monitorrecord in storage.
     * @param UpdateMonitorRecordRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateMonitorRecordRequest $request)
	{
		$monitorrecord = MonitorRecord::findOrFail($id);

        

		$monitorrecord->update($request->all());

		return redirect()->route(config('quickadmin.route').'.monitorrecord.index');
	}

	/**
	 * Remove the specified monitorrecord from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		MonitorRecord::destroy($id);

		return redirect()->route(config('quickadmin.route').'.monitorrecord.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            MonitorRecord::destroy($toDelete);
        } else {
            MonitorRecord::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.monitorrecord.index');
    }

}
