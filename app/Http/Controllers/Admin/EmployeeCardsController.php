<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\EmployeeCards;
use App\Http\Requests\CreateEmployeeCardsRequest;
use App\Http\Requests\UpdateEmployeeCardsRequest;
use Illuminate\Http\Request;

use App\Employee;


class EmployeeCardsController extends Controller {

	/**
	 * Display a listing of employeecards
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $employeecards = EmployeeCards::with("employee")->get();

		return view('admin.employeecards.index', compact('employeecards'));
	}

	/**
	 * Show the form for creating a new employeecards
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $employee = Employee::pluck("first_name", "id")->prepend('Please select', 0);

	    
	    return view('admin.employeecards.create', compact("employee"));
	}

	/**
	 * Store a newly created employeecards in storage.
	 *
     * @param CreateEmployeeCardsRequest|Request $request
	 */
	public function store(CreateEmployeeCardsRequest $request)
	{
	    
		EmployeeCards::create($request->all());

		return redirect()->route(config('quickadmin.route').'.employeecards.index');
	}

	/**
	 * Show the form for editing the specified employeecards.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$employeecards = EmployeeCards::find($id);
	    $employee = Employee::pluck("first_name", "id")->prepend('Please select', 0);

	    
		return view('admin.employeecards.edit', compact('employeecards', "employee"));
	}

	/**
	 * Update the specified employeecards in storage.
     * @param UpdateEmployeeCardsRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateEmployeeCardsRequest $request)
	{
		$employeecards = EmployeeCards::findOrFail($id);

        

		$employeecards->update($request->all());

		return redirect()->route(config('quickadmin.route').'.employeecards.index');
	}

	/**
	 * Remove the specified employeecards from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		EmployeeCards::destroy($id);

		return redirect()->route(config('quickadmin.route').'.employeecards.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            EmployeeCards::destroy($toDelete);
        } else {
            EmployeeCards::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.employeecards.index');
    }

}
