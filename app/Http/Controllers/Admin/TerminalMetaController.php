<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\TerminalMeta;
use App\Http\Requests\CreateTerminalMetaRequest;
use App\Http\Requests\UpdateTerminalMetaRequest;
use Illuminate\Http\Request;

use App\Terminals;


class TerminalMetaController extends Controller {

	/**
	 * Display a listing of terminalmeta
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $terminalmeta = TerminalMeta::with("terminals")->get();

		return view('admin.terminalmeta.index', compact('terminalmeta'));
	}

	/**
	 * Show the form for creating a new terminalmeta
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $terminals = Terminals::pluck("id", "id")->prepend('Please select', 0);

	    
        $terminal_type = TerminalMeta::$terminal_type;

	    return view('admin.terminalmeta.create', compact("terminals", "terminal_type"));
	}

	/**
	 * Store a newly created terminalmeta in storage.
	 *
     * @param CreateTerminalMetaRequest|Request $request
	 */
	public function store(CreateTerminalMetaRequest $request)
	{
	    
		TerminalMeta::create($request->all());

		return redirect()->route(config('quickadmin.route').'.terminalmeta.index');
	}

	/**
	 * Show the form for editing the specified terminalmeta.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$terminalmeta = TerminalMeta::find($id);
	    $terminals = Terminals::pluck("id", "id")->prepend('Please select', 0);

	    
        $terminal_type = TerminalMeta::$terminal_type;

		return view('admin.terminalmeta.edit', compact('terminalmeta', "terminals", "terminal_type"));
	}

	/**
	 * Update the specified terminalmeta in storage.
     * @param UpdateTerminalMetaRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateTerminalMetaRequest $request)
	{
		$terminalmeta = TerminalMeta::findOrFail($id);

        

		$terminalmeta->update($request->all());

		return redirect()->route(config('quickadmin.route').'.terminalmeta.index');
	}

	/**
	 * Remove the specified terminalmeta from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		TerminalMeta::destroy($id);

		return redirect()->route(config('quickadmin.route').'.terminalmeta.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            TerminalMeta::destroy($toDelete);
        } else {
            TerminalMeta::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.terminalmeta.index');
    }

}
