<?php
/**
 * Created by PhpStorm.
 * User: Jkirkpatrick
 * Date: 29/01/2019
 * Time: 15:02
 */

namespace App\Http\Controllers;

use App\Employee;
use App\MonitorRecord;
use App\TerminalMessage;
use Carbon\Carbon;
use Redirect;
use Schema;
use App\Terminals;
use App\Http\Requests\CreateTerminalsRequest;
use App\Http\Requests\UpdateTerminalsRequest;
use Illuminate\Http\Request;


class APITerminalsController extends Controller
{

    public function index()
    {
        $terminals = Terminals::with('TerminalMeta')->where("assigned", '=', '0')->get();

        return response()->json($terminals);
    }

    public function getTerminalData($id, Request $request){
        $terminal = Terminals::where('id', $id)->where('ip_address', $request->ip())->get();
        if (!empty(json_decode($terminal,1))){
            $shots = MonitorRecord::where('terminal_id', $id)
                ->whereDate('created_at', Carbon::today())
                ->where('record_type', 'Shot')
                ->count();

            $scrap = MonitorRecord::where('terminal_id', $id)
                ->whereDate('created_at', Carbon::today())
                ->where('record_type', 'scrap')
                ->count();

            $hourShot = MonitorRecord::where('terminal_id', $id)
                ->where('record_type', 'Shot')
                ->where('created_at', '>=', Carbon::now()->subHour())
                ->count();

            $hourScrap = MonitorRecord::where('terminal_id', $id)
                ->where('record_type', 'Scrap')
                ->where('created_at', '>=', Carbon::now()->subHour())
                ->count();

            $heats = MonitorRecord::select('record_data')
                ->where('terminal_id', $id)
                ->where('record_type', 'Heat')
                ->where('created_at', '>=', Carbon::now()->subMinutes(5))
                ->orderBy('created_at', 'desc')->first();

            $pumps = MonitorRecord::select('record_data')
                ->where('terminal_id', $id)
                ->where('record_type', 'Pump')
                ->where('created_at', '>=', Carbon::now()->subMinutes(5))
                ->orderBy('created_at', 'desc')->first();


            ($heats ? $heatState = $heats->record_data : $heatState = 0);
            ($pumps ? $pumpState = $pumps->record_data : $pumpState = 0);

            $data = array('shots' => $shots,
                            'hourShot' => $hourShot,
                            'hourScrap' => $hourScrap,
                            'scrap' => $scrap,
                'heats' => $heatState,
                'pumps' => $pumpState
            );
            return response()->json($data);

        }else{
            return response()->json(['Status' => 'No DATA, Wrong Terminal Settings']);
        }
    }


    public function getSwipeData($id, Request $request)
    {
        return response()->json("Test");
    }



    public function login()
    {
        return response()->json(Employee::doesntHave('Terminals')->orderBy('first_name')->get());
    }

    public function getTerminalMessage($terminalID)
    {
        $messages = TerminalMessage::where('terminal_id', $terminalID)->inRandomOrder()->first();

        return response()->json($messages);
    }



    public function claim($id)
    {
        $terminal = Terminals::where('id', $id)->where('assigned', 0)->update(['assigned' => 1]);

        if ($terminal == 1) {
            return response()->json(['Status' => 'success']);

        } else {
            return response()->json(['Status' => 'Unable to Claim Device']);

        }
    }

    /**
     * Update the specified terminals in storage.

     Post a json array with the following
    {
    "hostname": "Pen-Terminal-1",
    "mac_address": "as:ds:daa:we:as",
     *
    "ip_address": "192.168.0.36"
    }

     */
    public function update($id, Request $request)
    {
        $data = $request->all();


        $terminal = Terminals::where('id', $id)
            ->update(["hostname" => $data['hostname'],
                "mac_address" => $data['mac_address'],
                "ip_address" => $data['ip_address']]);

        if ($terminal == 1) {
            return response()->json(['Status' => 'success']);

        } else {
            return response()->json(['Status' => 'Unable to Claim Device']);
        }
    }

    public function heartbeat($id)
    {
        $terminals = Terminals::with('TerminalMeta')->where("id", '=', $id)->get();

        $terminals->terminalmeta->heartbeat = 1;


    }
}
