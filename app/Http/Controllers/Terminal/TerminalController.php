<?php

namespace App\Http\Controllers\Terminal;

use App\Employee;
use App\Http\Controllers\Controller;
use App\MonitorRecord;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Redirect;
use Schema;
use App\Terminals;
use App\Http\Requests\CreateTerminalsRequest;
use App\Http\Requests\UpdateTerminalsRequest;
use Illuminate\Http\Request;


class TerminalController extends Controller
{
    /**
     * Display a listing of terminals
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $terminal = Terminals::where("ip_address", $request->ip())->with('TerminalMeta')->firstOrFail();
            $hasEmployee = Terminals::has('Employee')
                ->where("ip_address", $request->ip())
                ->first();

            $employee = Employee::where('terminals_id', $terminal->id)->get();

            if (!$hasEmployee) {
                $employees = Employee::doesntHave('Terminals')->get();
                return view('terminals.login')->with('employees', $employees);
            }else{
                return view('terminals.index')->with(["terminal" => $terminal, "employee" => $employee]);
            }
        } catch (ModelNotFoundException $e) {
            return abort('403');
        }
    }


    public function login(Request $request)
    {
        $terminal = Terminals::where('ip_address', $request->ip())->first();
        Employee::where('id', $request->all()['Employee'])->update(['terminals_id' => $terminal->id]);


        $record = new MonitorRecord;
        $record->record_type = 'Login';
        $record->record_data = $request->Employee;
        $record->terminal_id = $terminal->id;
        $record->save();
#
        return  redirect()->route('terminalHome');
    }


    public function logout(Request $request)
    {
        $terminal = Terminals::where('ip_address', $request->ip())->first();
        Employee::where('id', $request->all()['Employee'])->update(['terminals_id' => null]);


        $record = new MonitorRecord;
        $record->record_type = 'Logout';
        $record->record_data = $request->Employee;
        $record->terminal_id = $terminal->id;
        $record->save();

        return  redirect()->route('terminalHome');

    }

    public function change_user(Request $request)
    {
        $terminal = Terminals::where('ip_address', $request->ip())->first();

        $old = $request->CurrentEmployee;
        $new = $request->newEmployee;

        Employee::where('id', $old)->update(['terminals_id' => null]);
        Employee::where('id', $new)->update(['terminals_id' => $terminal->id]);

        $record = new MonitorRecord;
        $record->record_type = 'Logout';
        $record->record_data = $old;
        $record->terminal_id = $terminal->id;
        $record->save();

        $record = new MonitorRecord;
        $record->record_type = 'Login';
        $record->record_data = $new;
        $record->terminal_id = $terminal->id;
        $record->save();

        return  redirect()->route('terminalHome');

    }


    public function change_employee()
    {
        
    }


    /**
     * Show the form for creating a new terminals
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('admin.terminals.create');
    }

    /**
     * Store a newly created terminals in storage.
     *
     * @param CreateTerminalsRequest|Request $request
     */
    public function store(CreateTerminalsRequest $request)
    {

        Terminals::create($request->all());

        return redirect()->route(config('quickadmin.route') . '.terminals.index');
    }

    /**
     * Show the form for editing the specified terminals.
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $terminals = Terminals::find($id);


        return view('admin.terminals.edit', compact('terminals'));
    }

    /**
     * Update the specified terminals in storage.
     * @param UpdateTerminalsRequest|Request $request
     *
     * @param  int $id
     */
    public function update($id, UpdateTerminalsRequest $request)
    {
        $terminals = Terminals::findOrFail($id);


        $terminals->update($request->all());

        return redirect()->route(config('quickadmin.route') . '.terminals.index');
    }

    /**
     * Remove the specified terminals from storage.
     *
     * @param  int $id
     */
    public function destroy($id)
    {
        Terminals::destroy($id);

        return redirect()->route(config('quickadmin.route') . '.terminals.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Terminals::destroy($toDelete);
        } else {
            Terminals::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route') . '.terminals.index');
    }

}
